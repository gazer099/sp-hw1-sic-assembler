from data import *

try:
    fin = open('input.txt', 'r')
except:
    print('ERROR: input.txt not found')
    exit(1)

def read_next_line():
    inList = fin.readline()
    symtab = inList[0:8]
    opcode = inList[8:16]
    operand = inList[16:]
    oneLineList = [symtab.strip(), opcode.strip(), operand.strip()]
    return oneLineList

# begin PASS1**********
interList = []

curLine = read_next_line()
if curLine[1] == 'START':
    startAddr = int(curLine[2], 16)
    locctr = int(curLine[2], 16)
    curLine.append(hex(locctr))
    interList.append(curLine)
    curLine = read_next_line()
else:
    locctr = 0

while curLine[1] != 'END':
    if curLine[0] != '.':
        curLine.append(hex(locctr))
        interList.append(curLine)
        # begin NOT A COMMENT PASS 1
        if curLine[0] != '':
            if curLine[0] in SYMTAB:
                print('ERROR: duplicate symbol')
                print(SYMTAB)
                errorFlag = 1
                exit(1)
            else:
                SYMTAB[curLine[0]] = hex(locctr)
        if curLine[1] in OPTAB:
            locctr += 3
        elif curLine[1] == 'WORD':
            locctr += 3
        elif curLine[1] == 'RESW':
            locctr += 3*int(curLine[2])
        elif curLine[1] == 'RESB':
            locctr += int(curLine[2])
        elif curLine[1] == 'BYTE':
            if curLine[2][0] == 'C':
                locctr += len(curLine[2])-3
            elif curLine[2][0] == 'X':
                locctr += int((len(curLine[2]) - 3)/2)
        else:
            print('ERROR: invalid operation code')
            errorFlag = 2
            exit(1)
        # end NOT A COMMENT PASS 1
    curLine = read_next_line()

curLine.append(hex(locctr))
interList.append(curLine)
print(interList)
pgmLen = locctr - startAddr
#print(hex(pgmLen))
print(SYMTAB)

fin.close()
# end PASS1**********

# begin PASS2**********
line = 0
if interList[line][1] == 'START':
    line += 1
headRecord = 'H{0:<6s}'.format(interList[0][0]) + interList[0][3][2:].zfill(6) + hex(pgmLen)[2:].zfill(6)
headRecord = headRecord.upper()
print(headRecord)

objectCodeList = []
while interList[line][1] != 'END':
    # begin NOT A COMMENT PASS 2
    if interList[line][1] in OPTAB:
        if interList[line][2] in SYMTAB:
            operandAddr = SYMTAB[interList[line][2]][2:]
        #else:
        #    operandAddr = 0
        #    print('ERROR: undefined symbol')
        #    exit(1)
        elif interList[line][2][-2:] == ',X':
            tmp = int(SYMTAB[interList[line][2][:-2]], 16)
            tmp += 32768
            operandAddr = hex(tmp)[2:]
        else:
            operandAddr = '0000'
        objectCode = OPTAB[interList[line][1]] + operandAddr
    elif interList[line][1] == 'BYTE':
        if interList[line][2][0] == 'C':
            objectCode = ''
            for i in interList[line][2][2:-1]:
                objectCode += hex(ord(i))[2:]
            objectc = objectCode.zfill(6)
        elif interList[line][2][0] == 'X':
                objectCode = interList[line][2][2:-1]
    elif interList[line][1] == 'WORD':
        objectCode = hex(int(interList[line][2]))[2:].zfill(6)
    else:
        objectCode = ''
    objectCode = objectCode.upper()
    objectCodeList.append(objectCode)
    #print(objectCode)
    line += 1
    # end NOT A COMMENT PASS 2
print(objectCodeList)

textRecordList = []
textLineTmp = 'T' + interList[0][3][2:].zfill(6)
cur = 0
while cur < len(objectCodeList):
    if textLineTmp == 'T':
        textLineTmp += textStartAddr
    else:
        if '' not in objectCodeList[cur:cur+10]:
            size = 0
            for i in range(cur, cur+10):
                if i >= len(objectCodeList):
                    break
                size += len(objectCodeList[i])
            size /= 2
            size = int(size)
            textLineTmp += hex(size)[2:].upper().zfill(2)
            for i in range(cur, cur+10):
                if i >= len(objectCodeList):
                    break
                textLineTmp += objectCodeList[i]
            textStartAddr = int(textLineTmp[1:7], 16)+int(textLineTmp[7:9], 16)
            textStartAddr = hex(textStartAddr)[2:].upper().zfill(6)
            textRecordList.append(textLineTmp)
            textLineTmp = 'T'
            cur += 10
        else:
            size = 0
            textLineTmp += '##'
            while objectCodeList[cur] != '':
                size += len(objectCodeList[cur])
                textLineTmp += objectCodeList[cur]
                cur += 1
            size /= 2
            size = int(size)
            textLineTmp = textLineTmp[0:7] + hex(size)[2:].upper().zfill(2) + textLineTmp[9:]
            reserveSize = 0
            while objectCodeList[cur] == '':
                if interList[cur+1][1] == 'RESW':
                    reserveSize += int(interList[cur+1][2])*3
                elif interList[cur+1][1] == 'RESB':
                    reserveSize += int(interList[cur+1][2])
                cur += 1
            textStartAddr = int(textLineTmp[1:7], 16) + int(textLineTmp[7:9], 16) + int(reserveSize)
            textStartAddr = hex(textStartAddr)[2:].upper().zfill(6)
            textRecordList.append(textLineTmp)
            textLineTmp = 'T'

print(textRecordList)
endRecord = 'E' + interList[0][3][2:].zfill(6)
print(endRecord)
# end PASS2**********

# begin write ObjectProgram.txt
fout = open('ObjectProgram.txt', 'w')
fout.write(headRecord+'\n')
for i in textRecordList:
    fout.write(i + '\n')
fout.write(endRecord+'\n')
fout.close()
# end write ObjectProgram.txt

# begin write ObjectList.txt
fout = open('ObjectList.txt', 'w')
counter = -1
for i in interList:
    fout.write('{0:<8s}'.format(i[3][2:]).upper())
    fout.write('{0:<8s}'.format(i[0]))
    fout.write('{0:<8s}'.format(i[1]))
    fout.write('{0:<12s}'.format(i[2]))
    if counter > -1 and counter < len(objectCodeList):
        fout.write('{0:<6s}\n'.format(objectCodeList[counter]))
    else:
        fout.write('\n')
    counter += 1
# end write ObjectList.txt

fout.close()
